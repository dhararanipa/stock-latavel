<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /**
     * login api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $data = $request->all();

        $email = $request->email;

        $user = User::whereEmail($email)->first();

        if (!$user) {
            $user = User::create($data);
        } else {
            $user->fill($data)->save();
        }

        $token = $user->createToken(Str::random(10))->accessToken;

        $user->fill(['api_token' => $token])->save();

        return response()->json(['token' => $token , 'user' => $user], 200);
    }
}
