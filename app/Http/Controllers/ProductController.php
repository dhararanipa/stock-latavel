<?php

namespace App\Http\Controllers;

use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        if (request()->ajax()) {

            $start_date = Carbon::parse(\request('start_date'))->format('d M Y');

            $end_date = (\request('end_date')) ? Carbon::parse(\request('end_date'))
                ->format('d M Y') : $start_date;

            $products = Product::whereBetween('date',[$start_date,$end_date])->get();

            return Datatables::of($products)
                ->make(true);
        }

        $disbl = '';
        $stocks = Product::where('date', now()->format('d M Y'))->get();

        if(count($stocks) > 0) {
            $disbl = 'disabled';
        }

        return view('product', compact('disbl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = now()->format('Y-m-d');
        $url = 'https://trendlyne.com/portfolio/bulk-block-deals/all/?defaultStockgroupjunk=all&start_date='.$date.'&end_date='.$date;
        // dd($url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $result = curl_exec($ch);

        $dom = new Dom;
        $dom->loadStr($result);
        $datas = $dom->find('.table-responsive > #bbdealTable > tbody');


        foreach ($datas as $data) {
            $dom = new Dom;
            $dom->loadStr($data->outerHtml);
            $trs = $dom->find('tr');

            foreach ($trs as $tr) {
                $dom = new Dom;
                $dom->loadStr($tr->outerHtml);
                $tds = $dom->find('td');

                $data = [];
                foreach ($tds as $td) {
                    array_push($data, $td->firstChild()->text);
                }

                if (count($data) === 9) {
                    Product::create([
                        'stock' => $data[0],
                        'client_name' => $data[1],
                        'action' => $data[4],
                        'date' => $data[5],
                        'price' => $data[6],
                        'quantity' => $data[7],
                        'pre_traded' => $data[8],
                    ]);
                }
            }
        }

        return redirect('/products');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
