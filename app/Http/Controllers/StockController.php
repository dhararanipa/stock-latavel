<?php

namespace App\Http\Controllers;

use App\Stock;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PHPHtmlParser\Dom;
use Yajra\DataTables\DataTables;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return
     */
    public function index()
    {
        if (request()->ajax()) {
            $start_date = Carbon::parse(\request('start_date'))->format('Y-m-d');

            $end_date = (\request('end_date')) ? Carbon::parse(\request('end_date'))
                ->format('Y-m-d') : $start_date;

            $filter = (\request('filter'));

            $stocks =  Stock::whereBetween('date',[$start_date,$end_date])->get();

            $filtered = $stocks->filter(function ($stock) {
                return $stock->close < $stock->sma200 && $stock->close > $stock->sma5 && $stock->close > $stock->sma10 && $stock->close > $stock->sma15 && $stock->close > $stock->sma20 && $stock->close > $stock->sma50 && $stock->close > $stock->sma100;
            });

            $datas = $filter === 'true' ? $filtered->all() : $stocks;

            return Datatables::of($datas)
                ->make(true);
        }
        $disbl = '';
        $stocks = Stock::whereDate('created_at', now()->format('Y-m-d'))->get();

        if(count($stocks) > 0) {
            $disbl = 'disabled';
        }

        return view('stock', compact('disbl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $date = now()->format('Y-m-d');
        $stocks = Stock::whereDate('created_at', $date)->get();

        if ($stocks->count() === 0) {
            $dom = new Dom;
            $url = 'https://www.topstockresearch.com/rt/IndexAnalyser/Nifty500/Technicals';
            $dom->loadFromUrl($url);
            $datas = $dom->find('table > tbody');

            foreach ($datas as $data) {
                $dom = new Dom;
                $dom->loadStr($data->outerHtml);
                $trs = $dom->find('tr');

                foreach ($trs as $tr) {
                    $dom = new Dom;
                    $dom->loadStr($tr->outerHtml);
                    $tds = $dom->find('td');

                    $data = [];
                    foreach ($tds as $td) {
                        array_push($data, $td->firstChild()->text);
                    }

                    if (count($data) === 9) {
                        Stock::create([
                            'name' => $data[0],
                            'close' => $data[1],
                            'sma5' => $data[2],
                            'sma10' => $data[3],
                            'sma15' => $data[4],
                            'sma20' => $data[5],
                            'sma50' => $data[6],
                            'sma100' => $data[7],
                            'sma200' => $data[8],
                            'date' => now()->format('Y-m-d')
                        ]);
                    }
                }
            }
        }

        return redirect('/stocks');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param null $start
     * @param null $end
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function show($start = null, $end = null)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
