<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'author' => $this->author,
            'tags' => $this->tags ? explode(', ', $this->tags) : [],
            'categories' => $this->categories ? explode(', ', $this->categories) : [],
            'like_count' => (int)$this->likesCount,
        ];
    }
}
