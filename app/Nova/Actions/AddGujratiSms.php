<?php

namespace App\Nova\Actions;

use App\Stock;
use Illuminate\Bus\Queueable;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use PHPHtmlParser\Dom;

class AddGujratiSms extends Action
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Perform the action on the given models.
     *
     * @param \Laravel\Nova\Fields\ActionFields $fields
     * @param \Illuminate\Support\Collection $models
     * @return mixed
     * @throws \PHPHtmlParser\Exceptions\ChildNotFoundException
     * @throws \PHPHtmlParser\Exceptions\CircularException
     * @throws \PHPHtmlParser\Exceptions\CurlException
     * @throws \PHPHtmlParser\Exceptions\NotLoadedException
     * @throws \PHPHtmlParser\Exceptions\StrictException
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        $dom = new Dom;
        $url = 'https://www.topstockresearch.com/rt/IndexAnalyser/Nifty500/Technicals';
        $dom->loadFromUrl($url);
        $datas = $dom->find('table > tbody');

        foreach ($datas as $data) {
            $dom = new Dom;
            $dom->loadStr($data->outerHtml);
            $trs = $dom->find('tr');

            foreach ($trs as $tr) {
                $dom = new Dom;
                $dom->loadStr($tr->outerHtml);
                $tds = $dom->find('td');

                $data = [];
                foreach ($tds as $td) {
                    array_push($data, $td->firstChild()->text);
                }

                if (count($data) === 9) {
                    Stock::create([
                        'name' => $data[0],
                        'close' => $data[1],
                        'sma5' => $data[2],
                        'sma10' => $data[3],
                        'sma15' => $data[4],
                        'sma20' => $data[5],
                        'sma50' => $data[6],
                        'sma100' => $data[7],
                        'sma200' => $data[8]
                    ]);
                }
            }
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [];
    }
}
