<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'date', 'stock', 'client_name', 'action', 'quantity', 'price', 'pre_traded'
    ];
}
