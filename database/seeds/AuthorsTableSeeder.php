<?php

use App\Authors;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/authors.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            Authors::create($data);
        }
    }
}
