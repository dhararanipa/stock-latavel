<?php

use App\Background;
use Illuminate\Database\Seeder;

class BackgroundsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/backgrounds.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            Background::create($data);
        }
    }
}
