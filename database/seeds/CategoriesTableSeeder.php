<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/categories.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            Category::create($data);
        }
    }
}
