<?php

use App\Frame;
use Illuminate\Database\Seeder;

class FramesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/frames.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            Frame::create($data);
        }
    }
}
