<?php

use App\PreviewTheme;
use Illuminate\Database\Seeder;

class PreviewThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/themes-preview.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            $data['extra'] = json_encode($data['extra']);

            PreviewTheme::create($data);
        }
    }
}
