<?php

use App\Quote;
use Illuminate\Database\Seeder;

class QuotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/quotes.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            $data['tags'] = implode(", ", $data['tags']);

            $data['categories'] = implode(", ", $data['categories']);

            Quote::create($data);
        }
    }
}
