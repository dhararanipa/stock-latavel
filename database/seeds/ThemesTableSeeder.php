<?php

use App\Theme;
use Illuminate\Database\Seeder;

class ThemesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jsonString = file_get_contents(base_path('database/Json/themes.json'));

        $datas = json_decode($jsonString, true);

        foreach($datas as $data){
            $data1['data'] =json_encode($data);

            Theme::create($data1);
        }
    }
}
