@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center mb-3">
                            <strong>Stocks</strong>
{{--                            <div class="input-group-text ml-2 mr-4">--}}
{{--                                <input type="checkbox" value="true" id="defaultCheck" checked>--}}
{{--                            </div>--}}
                            <date-picker id="date-time" class=" ml-auto"></date-picker>
                            <a href="" id="btn-filter" type="button" class="btn btn-primary px-3 mx-3">Go</a>
                            <a href="/products/create" type="button" class="btn btn-primary {{ $disbl }}" >ADD</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" id="stocks-table">
                            <thead>
                            <tr>
                                <th>Date</th>
                                <th>Stock</th>
                                <th>Client Name</th>
                                <th>Buy/Sell</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Percentage Traded %</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            let dt = $('#stocks-table').DataTable({
                processing: true,
                serverSide: true,
                lengthMenu: [[100, 200, 400, 600, 800, 1000], [100, 200, 400, 600, 800, 1000]],
                pageLength: 600,
                ajax: {
                    'url': '{!! route('products.index') !!}',
                    'data': function ( d ) {
                        d.start_date = $('#start-date').val()
                        d.end_date = $('#end-date').val()
                        d.filter = $('#defaultCheck').val()
                    }
                },
                columns: [
                    { data: 'date', name: 'date' },
                    { data: 'stock', name: 'stock' },
                    { data: 'client_name', name: 'client_name' },
                    { data: 'action', name: 'action' },
                    { data: 'quantity', name: 'quantity' },
                    { data: 'price', name: 'price' },
                    { data: 'pre_traded', name: 'pre_traded' },
                ]
            });

            $('#btn-filter').click(function(e){
                e.preventDefault();
                dt.draw()
            });

            $('#defaultCheck').click(function (e) {
                $('#defaultCheck').attr('checked', this.checked);
                $('#defaultCheck').attr('value', this.checked);
                // e.preventDefault();
                dt.draw()
            })
        });
    </script>
@endpush
