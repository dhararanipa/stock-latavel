@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex align-items-center mb-3">
                            <strong>Stocks</strong>
                            <div class="input-group-text ml-2 mr-4">
                                <input type="checkbox" value="true" id="defaultCheck" checked>
                            </div>
                            <date-picker id="date-time" class=" ml-auto"></date-picker>
                            <a href="" id="btn-filter" type="button" class="btn btn-primary px-3 mx-3">Go</a>
                            <a href="/stocks/create" type="button" class="btn btn-primary {{ $disbl }}" >ADD</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered" id="stocks-table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Close</th>
                                <th>SMA 5</th>
                                <th>SMA 10</th>
                                <th>SMA 15</th>
                                <th>SMA 20</th>
                                <th>SMA 50</th>
                                <th>SMA 100</th>
                                <th>SMA 200</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(function() {
            let dt = $('#stocks-table').DataTable({
                processing: true,
                serverSide: true,
                lengthMenu: [[100, 200, 400, 600, 800, 1000], [100, 200, 400, 600, 800, 1000]],
                pageLength: 600,
                ajax: {
                    'url': '{!! route('stocks.index') !!}',
                    'data': function ( d ) {
                        d.start_date = $('#start-date').val()
                        d.end_date = $('#end-date').val()
                        d.filter = $('#defaultCheck').val()
                    }
                },
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'close', name: 'close' },
                    { data: 'sma5', name: 'sma5' },
                    { data: 'sma10', name: 'sma10' },
                    { data: 'sma15', name: 'sma15' },
                    { data: 'sma20', name: 'sma20' },
                    { data: 'sma50', name: 'sma50' },
                    { data: 'sma100', name: 'sma100' },
                    { data: 'sma200', name: 'sma200' },
                ]
            });

            $('#btn-filter').click(function(e){
                e.preventDefault();
                dt.draw()
            });

            $('#defaultCheck').click(function (e) {
                $('#defaultCheck').attr('checked', this.checked);
                $('#defaultCheck').attr('value', this.checked);
                // e.preventDefault();
                dt.draw()
            })
        });
    </script>
@endpush
