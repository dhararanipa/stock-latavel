<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/auth/login', 'Auth\LoginController@login');

Route::post('/login', 'Api\Auth\LoginController@login');

Route::namespace('Api')->group(function () {
    Route::post('/author', 'AuthorsController@store');
    Route::get('/author', 'AuthorsController@index');

    Route::post('/category', 'CategoryController@store');
    Route::get('/category', 'CategoryController@index');

    Route::post('/quote', 'QuoteController@store');
    Route::get('/quote', 'QuoteController@index');
    Route::get('/quotes/author/{author}', 'QuoteController@fetchAuthor');
    Route::get('/quotes/category/{category}', 'QuoteController@fetchCategory');
    Route::get('/quotes/{text}', 'QuoteController@fetch');
    Route::post('/quotes/like', 'QuoteController@like');

    Route::post('/background', 'BackgroundController@store');
    Route::get('/background', 'BackgroundController@index');

    Route::post('/frame', 'FrameController@store');
    Route::get('/frame', 'FrameController@index');

    Route::post('/themes', 'ThemeController@store');
    Route::get('/themes', 'ThemeController@index');
    Route::get('/themes/{id}', 'ThemeController@fetch');
    Route::delete('/themes/{id}', 'ThemeController@delete');
    Route::get('/themes-filter', 'ThemeController@filter');

    Route::post('/tags', 'TagController@store');
    Route::get('/search-tags', 'TagController@index');

    Route::get('/hindi-quotes', 'HindiQuoteController@index');
    Route::post('/hindi-quotes/like', 'HindiQuoteController@like')->middleware('auth:api');
    Route::post('/hindi-quotes/share', 'HindiQuoteController@share');

    Route::get('/gujarati-quotes', 'GujaratiQuoteController@index');
    Route::post('/gujarati-quotes/like', 'GujaratiQuoteController@like')->middleware('auth:api');
    Route::post('/gujarati-quotes/share', 'GujaratiQuoteController@share');

    Route::post('/template', 'TemplateController@store');
    Route::get('/template', 'TemplateController@index');

    Route::post('/feedback', 'FeedbackController@store');
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

